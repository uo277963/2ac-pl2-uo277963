	.data

string1:	.asciiz "something"
result:		.word 0

	.code
	
main:
	xor r8, r8, r8	;current number of characters
	xor r9, r9, r9	;memory index of current item
	
loop:
	lbu r4, string1(r9)
	beqz r4, end
	daddi r8, r8, 1
	daddui r9, r9, 1
	j loop
	
	
	
end:
	sd r8, result(r0)
	halt