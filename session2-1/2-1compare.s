	.data

string1:	.asciiz "something"
string2:	.asciiz "somethingelse"

	.code
main:
	xor r15, r15, r15	;string 1 length
	xor r16, r16, r16	;string 2 length
	xor r8, r8, r8		;0 if different, 1 if equal
	xor r4, r4, r4
	
	daddi r4, r4, string1
	jal procedure
	dadd r15, r15, r2
	
	daddi r4, r4, string2
	jal procedure
	dadd r16, r16, r2
	

	
	beq r15, r16, equal
	
	j end
	
	
equal:
	daddi r8, r8, 1
	j end
	
	
procedure:
	;recieves a string address in r4 and returns its length in r2. changes the value of r9
	xor r9, r9, r9
	xor r2, r2, r2
	
	loop:
		xor r3, r3, r3
		dadd r3, r3, r4
		
		dadd r3, r3, r9
		
		lbu r5, 0(r3)
		beqz r5, endprocedure
		daddi r2, r2, 1
		daddi r9, r9, 1 ;r9 += 1
		
		j loop
	endprocedure:
		jr r31

end:
	halt